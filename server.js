var Express  = require('express')
    , App = Express();
var BodyParser = require('body-parser');
var fs = require('fs');
var Redis = require("redis"),
    Client = Redis.createClient();
var Promise = require('promise');

var dataFile = './data/jsonFile.json';

Client.on("error", function (err) {
    console.log("Error " + err);
});

App.use(BodyParser.json());

function appendToFile(data) {
    return new Promise(function (fulfill, reject){
        fs.appendFile(dataFile, data, function (err, res) {
            if (err) {
                reject(err);
            } else {
                fulfill(res);
            }
        });
    });
}

function redisGet(key){
    return new Promise(function (fulfill, reject){
        Client.get(key, function(err, count) {
            if (err){
                reject(err);
            }else{
                fulfill(count);
            }
        });
    });
}

function redisSet(key, value){
    return new Promise(function (fulfill, reject){
        Client.set(key, value, function(err, count) {
            if (err){
                reject(err);
            }else{
                fulfill(count);
            }
        });
    });
}

function increaseCount(count) {
    return new Promise(function (fulfill, reject){
        if (typeof count !== 'undefined'){
            redisGet("count").then(function(returnCount){
                var newCount = returnCount ? parseInt(returnCount)+1 : 1;
                redisSet("count", newCount).then(function(returnCount){
                    fulfill(true);
                }, function(err){
                    reject(err);
                });
            }, function(err) {
                reject(err);
            });
        }else{
            fulfill(false);
        }
    });
}

App.get('/', function(req, res){
    var html = fs.readFileSync('index.html');
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.end(html);
});

App.get('/count', function(req, res){
    redisGet("count").then(function(count){
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify({ count: count }));
    });
});

App.post('/track', function(req, res){
    var body = req.body;

    var promiseAppendToFile = appendToFile(JSON.stringify(body));
    var promiseAddToRedis = increaseCount(body.count);

    Promise.all([promiseAppendToFile, promiseAddToRedis]).then(function(){
        res.setHeader("Content-Type", "application/json");
        res.send({ "status": "done" });
        res.writeHead(200);
        res.end();
    }, function(err){
        console.log(err);
        res.setHeader("Content-Type", "application/json");
        res.send({ "status": "error" });
        res.writeHead(500);
        res.end();
    });

});

App.listen(1234);
console.log("Server is running at 1234");