var chai = require('chai');
var chaiHttp = require("chai-http");
var chaiFs = require('chai-fs');
var fs = require('fs');
var should = chai.should();

chai.use(chaiFs);
chai.use(chaiHttp);

it('#1 test GET on /count', function(done) {
    var server = chai.request("http://localhost:1234");
    server.get('/count').end(function(err, res){
        res.should.have.status(200);
        res.should.be.json;
        res.body.should.be.a('object');
        res.body.should.have.property('count');
        var count = res.body.count;
        server.get('/count').end(function(err2, res2){
            res2.should.have.status(200);
            res2.should.be.json;
            res2.body.should.be.a('object');
            res2.body.should.have.property('count');
            res2.body.count.should.equal(count);
            done();
        });

    });
});

it('#2 test POST on /track count', function(done) {
    var server = chai.request("http://localhost:1234");
    server.get('/count').end(function(err, res){
        res.should.have.status(200);
        res.should.be.json;
        res.body.should.be.a('object');
        res.body.should.have.property('count');
        var count = res.body.count;
        var path = './data/jsonFile.json';
        fs.readFile(path, function read(err, fileContent) {
            if (err) {throw err;}

            var data = { title: 'Title', description: 'Description', count: true };
            server.post('/track').send(data).end(function(err2, res2){
                res2.should.have.status(200);
                res2.should.be.a('object');
                res2.body.should.have.property('status');

                path.should.be.a.file().with.content(fileContent+JSON.stringify(data));
                server.get('/count').end(function(err3, res3){
                    res3.should.have.status(200);
                    res3.should.be.json;
                    res3.body.should.be.a('object');
                    res3.body.should.have.property('count');
                    res3.body.count.should.equal((parseInt(count)+1).toString());
                    done();
                });
            });
        });
    });
});